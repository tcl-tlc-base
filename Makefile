DESTDIR=

TEAPOT=/var/teapot
MYDESTDIR=$(DESTDIR)/usr/lib/tlc-base

TCLSH = TCLLIBPATH="." tclsh8.5

all: scripts

scripts: scripts-stamp

scripts-stamp: scripts/*.tcl scripts/*.itcl
	./make_tclIndex.tcl
	touch scripts-stamp

install: all
	install -d $(MYDESTDIR)
	install -d $(MYDESTDIR)/scripts
	install pkgIndex.tcl $(MYDESTDIR)
	install init.tcl $(MYDESTDIR)
	install scripts/*.tcl $(MYDESTDIR)/scripts
	install scripts/*.itcl $(MYDESTDIR)/scripts
	install scripts/tclIndex $(MYDESTDIR)/scripts

teapot: all
	install -d teapot
	-rm -f teapot/*
	teapot-pkg generate --output teapot --type zip .

install-teapot: teapot
	teapot-admin add $(TEAPOT) teapot/*.zip

refresh-teapot: teapot
	teacup remove TLC-base
	teacup install teapot/*

uninstall:
	-rm -rf $(MYDESTDIR)

test: all
	$(TCLSH) tests/all.tcl $(TESTFLAGS)

clean:
	-rm -rf scripts/tclIndex scripts-stamp teapot
