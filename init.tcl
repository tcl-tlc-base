package require Tcl 8.5
package require Itcl 3.3

namespace eval ::tlc {
	namespace export *

	variable library \
			[file normalize [file join [pwd] [file dirname [info script]]]]

	variable version_base	0.98.1

	variable log
	set log(threshold)	20
}

source [file join $::tlc::library scripts intersect3.tcl]
lappend auto_path [file join $::tlc::library scripts]

tlc::Uri tlc::uri

namespace import -force itcl::*

package provide TLC-base $::tlc::version_base


