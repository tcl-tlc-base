# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

if {[lsearch [namespace children] ::tcltest] == -1} {
	package require tcltest 2.2.5
	namespace import ::tcltest::*
}

package require TLC-base

test signals-1.1 {Create a Signal} -body { #<<<
	tlc::Signal #auto signal
	$signal isa tlc::Signal
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -result {1}
#>>>
test signals-1.2 {Test initial state is false} -body { #<<<
	tlc::Signal #auto signal
	$signal state
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -result {0}
#>>>
test signals-1.3 {Setting state true} -body { #<<<
	tlc::Signal #auto signal
	$signal set_state 1
	$signal state
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -result {1}
#>>>
test signals-1.4 {State normalization} -body { #<<<
	tlc::Signal #auto signal
	$signal set_state true
	$signal state
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -result {1}
#>>>
test signals-1.5 {Rejection of non booleans} -body { #<<<
	tlc::Signal #auto signal
	$signal set_state maybe
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -returnCodes {
	error
} -result {newstate must be a valid boolean}
#>>>
test signals-1.6 {Toggle state} -body { #<<<
	tlc::Signal #auto signal
	$signal toggle_state
	$signal state
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
} -result {1}
#>>>
test signals-1.7 {Auto lifecycle management} -body { #<<<
	tlc::Signal #auto signal
	set hold	$signal
	set before	[itcl::is object $hold]
	unset signal
	set after	[itcl::is object $hold]
	list $before $after
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
	if {[info exists hold]} {
		unset hold
	}
} -result {1 0}
#>>>
test signals-1.8 {State change callback} -body { #<<<
	tlc::Signal #auto signal
	$signal attach_output {apply {
		{newstate} {
			set ::state_from_callback	$newstate
		}
	}}
	$signal set_state 1
	set ::state_from_callback
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
	if {[info exists ::state_from_callback]} {
		unset ::state_from_callback
	}
} -result {1}
#>>>
test signals-1.9 {State change callback, state initialization} -body { #<<<
	tlc::Signal #auto signal
	$signal attach_output {apply {
		{newstate} {
			set ::state_from_callback	$newstate
		}
	}}
	set ::state_from_callback
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
	if {[info exists ::state_from_callback]} {
		unset ::state_from_callback
	}
} -result {0}
#>>>
test signals-1.10 {State change callback, state propagation optimization} -body { #<<<
	tlc::Signal #auto signal
	$signal attach_output {apply {
		{newstate} {
			lappend ::state_from_callback	$newstate
		}
	}}
	$signal set_state 1
	$signal set_state 1
	$signal set_state 0
	$signal set_state 0
	set ::state_from_callback
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
	if {[info exists ::state_from_callback]} {
		unset ::state_from_callback
	}
} -result {0 1 0}
#>>>
test signals-1.11 {Detatch state change callback} -body { #<<<
	tlc::Signal #auto signal
	set handler	{apply {
		{newstate} {
			lappend ::state_from_callback	$newstate
		}
	}}
	$signal attach_output $handler
	$signal set_state 1
	$signal detach_output $handler

	$signal set_state 0
	set ::state_from_callback
} -cleanup {
	if {[info exists signal] && [itcl::is object $signal]} {
		delete object $signal
		unset signal
	}
	if {[info exists ::state_from_callback]} {unset ::state_from_callback}
	if {[info exists handler]} {unset handler}
} -result {0 1}
#>>>
test signals-1.12 {Detatch from outputs at death} -body { #<<<
	tlc::Signal #auto signals(1)
	tlc::Signal #auto signals(2)
	tlc::Gate #auto signals(gate) -mode and

	$signals(gate) attach_input $signals(1)
	$signals(gate) attach_input $signals(2)

	$signals(2) set_state 1

	set before	[$signals(gate) state]
	array unset signals 1
	set after	[$signals(gate) state]

	list $before $after
} -cleanup {
	array unset signals
	if {[info exists before]} {unset before}
	if {[info exists after]} {unset after}
} -result {0 1}
#>>>
test signals-1.13 {Handling of -name} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"
	$signals(1) name
} -cleanup {
	array unset signals
} -result {Test signal}
#>>>
test signals-1.14 {explain_txt method} -body { #<<<
	tlc::Signal sigtest-1.14 signals(1) -name "Test signal"
	list [$signals(1) explain_txt] [$signals(1) explain_txt 2]
} -cleanup {
	array unset signals
} -result {{::sigtest-1.14 "Test signal": 0
} {    ::sigtest-1.14 "Test signal": 0
}}
#>>>
test signals-1.15 {waitfor, target state (false) already set} -body { #<<<
	tlc::Signal #auto signals(1)
	set killed	0

	set afterid	[after 100 [list apply {
		{obj} {
			set ::killed	1
			delete object	$obj
		}
	} $signals(1)]]

	$signals(1) waitfor false
	list $killed [$signals(1) state]
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists killed]} {
		unset killed
	}
	array unset signals
} -result {0 0}
#>>>
test signals-1.16 {waitfor, target state (true) already set} -body { #<<<
	tlc::Signal #auto signals(1)
	set killed	0

	$signals(1) set_state 1

	set afterid	[after 100 [list apply {
		{obj} {
			set ::killed	1
			delete object	$obj
		}
	} $signals(1)]]

	$signals(1) waitfor true
	list $killed [$signals(1) state]
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists killed]} {
		unset killed
	}
	array unset signals
} -result {0 1}
#>>>
test signals-1.17 {waitfor, target state (false) not already set} -body { #<<<
	tlc::Signal #auto signals(1)
	$signals(1) set_state 1

	set before	[clock milliseconds]

	set timebomb	[after 1000 [list apply {
		{obj} {
			puts "signals-1.17: timebomb went off"
			if {[itcl::is object $obj]} {
				delete object $obj
			}
		}
	} $signals(1)]]

	set afterid	[after 200 [list apply {
		{obj} {
			$obj set_state 0
		}
	} $signals(1)]]

	$signals(1) waitfor false
	set after	[clock milliseconds]
	expr {$after - $before > 180}
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists timebomb]} {
		after cancel $timebomb
		unset timebomb
	}
	array unset signals
} -result {1}
#>>>
test signals-1.18 {waitfor, target state (true) not already set} -body { #<<<
	tlc::Signal #auto signals(1)

	set before	[clock milliseconds]

	set timebomb	[after 1000 [list apply {
		{obj} {
			puts "signals-1.18: timebomb went off"
			if {[itcl::is object $obj]} {
				delete object $obj
			}
		}
	} $signals(1)]]

	set afterid	[after 200 [list apply {
		{obj} {
			$obj set_state 1
		}
	} $signals(1)]]

	$signals(1) waitfor true
	set after	[clock milliseconds]
	expr {$after - $before > 180}
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists timebomb]} {
		after cancel $timebomb
		unset timebomb
	}
	array unset signals
} -result {1}
#>>>
test signals-1.19 {waitfor timeout, timeout reached} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set timebomb	[after 1000 [list apply {
		{obj} {
			puts "signals-1.19: timebomb went off"
			if {[itcl::is object $obj]} {
				delete object $obj
			}
		}
	} $signals(1)]]

	set before	[clock milliseconds]

	catch {
		$signals(1) waitfor true 200
	} errmsg options
	set options	[dict merge {-errorcode ""} $options]

	set after	[clock milliseconds]
	list [expr {$after - $before >= 200}] $errmsg [dict get $options -errorcode]
} -cleanup {
	if {[info exists timebomb]} {
		after cancel $timebomb
		unset timebomb
	}
	foreach var {before after} {
		if {[info exists $var]} {
			unset $var
		}
	}
	array unset signals
} -result {1 {Timeout waiting for signal "Test signal"} {timeout {Test signal}}}
#>>>
test signals-1.20 {waitfor timeout, timeout not reached} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set timebomb	[after 1000 [list apply {
		{obj} {
			puts "signals-1.20: timebomb went off"
			if {[itcl::is object $obj]} {
				delete object $obj
			}
		}
	} $signals(1)]]

	set afterid		[after 100 [list $signals(1) set_state 1]]

	set before	[clock milliseconds]
	$signals(1) waitfor true 200
	set after	[clock milliseconds]

	list [expr {$after - $before < 150}] [$signals(1) state]
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists timebomb]} {
		after cancel $timebomb
		unset timebomb
	}
	foreach var {before after} {
		if {[info exists $var]} {
			unset $var
		}
	}
	array unset signals
} -result {1 1}
#>>>
test signals-1.21 {waitfor timeout, died before timeout or state reached} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set timebomb	[after 100 [list apply {
		{obj} {
			if {[itcl::is object $obj]} {
				delete object $obj
			}
		}
	} $signals(1)]]

	set afterid		[after 200 [list apply {
		{obj} {
			if {[itcl::is object $obj]} {
				$obj set_state 1
			}
		}
	} $signals(1)]]

	catch {
		$signals(1) waitfor true 1000
	} errmsg options
	set options	[dict merge {-errorcode ""} $options]

	list $errmsg [dict get $options -errorcode]
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	if {[info exists timebomb]} {
		after cancel $timebomb
		unset timebomb
	}
	array unset signals
} -result {{Source died while waiting for signal "Test signal"} {source_died {Test signal}}}
#>>>
test signals-1.22 {waitfor timeout, state flop race} -body { #<<<
	tlc::Signal ::#auto signals(1) -name "Test signal"

	set afterid	[after 100 [list apply {
		{obj} {
			$obj set_state 1
			$obj set_state 0
		}
	} $signals(1)]]

	catch {
		$signals(1) waitfor true 1000
	} errmsg options
	set options	[dict merge {-errorcode ""} $options]

	list $errmsg [dict get $options -errorcode]
} -cleanup {
	if {[info exists afterid]} {
		after cancel $afterid
		unset afterid
	}
	array unset signals
} -result {{Timeout waiting for signal "Test signal"} {timeout {Test signal}}} -match glob -errorOutput {*Woken up by transient spike while waiting for state true, waiting for more permanent change*}
#>>>
test signals-1.23 {Avoid double-adding output handler} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set handler	{apply {
		{newstate} {}
	}}

	set first	[$signals(1) attach_output $handler]
	set second	[$signals(1) attach_output $handler]

	list $first $second
} -cleanup {
	foreach var {first second} {
		if {[info exists $var]} {
			unset $var
		}
	}

	array unset signals
} -result {1 0}
#>>>
test signals-1.24 {Detect double-removing output handler} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set handler	{apply {
		{newstate} {}
	}}

	$signals(1) attach_output $handler
	set first	[$signals(1) detach_output $handler]
	set second	[$signals(1) detach_output $handler]

	list $first $second
} -cleanup {
	foreach var {first second} {
		if {[info exists $var]} {
			unset $var
		}
	}

	array unset signals
} -result {1 0}
#>>>
test signals-1.25 {Catch error in output handler} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal"

	set good_output_ok	0

	$signals(1) attach_output {apply {
		{newstate} {
			error "Test error"
		}
	}}

	$signals(1) attach_output {apply {
		{newstate} {
			set ::good_output_ok	1
		}
	}}

	set good_output_ok
} -cleanup {
	foreach var {good_output_ok} {
		if {[info exists $var]} {
			unset $var
		}
	}

	array unset signals
} -result {1} -match glob -errorOutput {*"Test signal" error updating output (0) handler: (apply *Test error*}
#>>>
test signals-1.26 {Debug mode output} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal" -debugmode 1 -output_handler_warntime 200

	$signals(1) register_handler debug {apply {
		{level msg} {
			puts $msg
		}
	}}

	$signals(1) attach_output {apply {
		{newstate} {
			if {$newstate} {
				set ::foo	0
				set ::afterids(1)	[after 700 {set ::foo 1}]
				vwait ::foo
				unset ::foo
			}
		}
	}}

	set hold	$signals(1)

	set afterids(2)	[after 100 [list array unset signals 1]]
	$signals(1) set_state 1

	itcl::is object $hold
} -cleanup {
	foreach {num handle} [array get afterids] {
		after cancel $handle
	}
	array unset afterids

	array unset signals

	foreach var {hold foo} {
		if {[info exists $var]} {
			unset $var
		}
	}
} -result {0} -match glob -output {*tlc::Signal::scopevar_unset:
::tlc::Signal::scopevar_unset(args signals 1 u)
*
tlc::Signal::destructor: ::signal* Test signal dieing
tlc::Signal::destructor: ::signal* dieing from:
::tlc::Signal::destructor(<undefined> {})
::tlc::Signal::scopevar_unset(args signals 1 u)
*
tlc::Signal::destructor: ------ twitch: (apply {
*})
tlc::Signal::detach_output: (apply {
*})
tlc::Signal::destructor: ::signal* truely dead
tlc::Signal::update_outputs: Flagging changewaits: ()
*}
#>>>
test signals-1.27 {Feedback for slow output handlers} -body { #<<<
	tlc::Signal #auto signals(1) -name "Test signal" -debugmode 1 -output_handler_warntime 200

	$signals(1) attach_output {apply {
		{newstate} {
			if {$newstate} {
				set ::foo	0
				set ::afterids(1)	[after 700 {set ::foo 1}]
				vwait ::foo
				unset ::foo
			}
		}
	}}

	$signals(1) set_state 1

	$signals(1) state
} -cleanup {
	foreach {num handle} [array get afterids] {
		after cancel $handle
	}
	array unset afterids

	array unset signals

	foreach var {foo} {
		if {[info exists $var]} {
			unset $var
		}
	}
} -result {1} -match glob -errorOutput {*name: (Test signal) obj: (*) taking way too long to update output for handler: (apply *}
#>>>

::tcltest::cleanupTests
return


