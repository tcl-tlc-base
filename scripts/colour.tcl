# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

proc tlc::colour {args} {
	if {$::tcl_platform(platform) == "windows"} {return ""}
	set build	""
	foreach name $args {
		switch -- [string tolower $name] {
			"black"		{append build "\[30m"}
			"red"		{append build "\[31m"}
			"green"		{append build "\[32m"}
			"yellow"	{append build "\[33m"}
			"blue"		{append build "\[34m"}
			"purple"	{append build "\[35m"}
			"cyan"		{append build "\[36m"}
			"white"		{append build "\[37m"}
			"bg_black"	{append build "\[40m"}
			"bg_red"	{append build "\[41m"}
			"bg_green"	{append build "\[42m"}
			"bg_yellow"	{append build "\[43m"}
			"bg_blue"	{append build "\[44m"}
			"bg_purple"	{append build "\[45m"}
			"bg_cyan"	{append build "\[46m"}
			"bg_white"	{append build "\[47m"}
			"inverse"	{append build "\[7m"}
			"bold"		{append build "\[5m"}
			"underline"	{append build "\[4m"}
			"bright"	{append build "\[1m"}
			"norm"		{append build "\[0m"}
			default		{append build ""}
		}
	}

	return $build
}


