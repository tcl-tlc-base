proc tlc::assert {expr msg} {
	if {![uplevel [list expr $expr]]} {
		error "Assert failed: \"$msg\"" "" [list assert_failed $expr $msg]
	} else {
		return 1
	}
}


