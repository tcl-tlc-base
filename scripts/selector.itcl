# vim: foldmarker=<<<,>>>

class tlc::Selector {
	inherit tlc::Signal

	constructor {accessvar args} {
		upvar $accessvar scopevar
		eval tlc::Signal::constructor scopevar
	} {}
	destructor {}

	public {
		variable default	0

		method attach_input {token signal {a_sense normal}}
		method detach_input {token}
		method select {token}
		method explain_txt {{depth 0}}
	}

	private {
		variable inputs
		variable sense
		variable selected_token		""

		method input_update {token newstate}
		method cleanup {token}
		method input_debug {lvl msg}
	}
}


body tlc::Selector::constructor {accessvar args} { #<<<1
	array set inputs	{}
	array set sense		{}

	eval configure $args
}


body tlc::Selector::attach_input {token signal {a_sense normal}} { #<<<1
	if {[info exists inputs($token)]} {
		detach_input $token
	}

	set inputs($token)	$signal
	set sense($token)	[expr {$a_sense != "normal"}]

	#$signal register_handler debug [code $this input_debug]

	return [$signal attach_output [code $this input_update $token] \
			[code $this cleanup $token]]
}


body tlc::Selector::detach_input {token} { #<<<1
	puts stderr "tlc::Selector::detach_input: name: ($name) token: ($token)"
	$inputs($token) detach_output [code $this input_update $token]

	array unset inputs $token
	array unset sense $token

	if {$token == $selected_token} {
		set selected_token	""
		set_state $default
	}
}


body tlc::Selector::input_update {token newstate} { #<<<1
	if {$token == $selected_token} {
		if {$sense($token)} {
			set_state [expr {!($newstate)}]
		} else {
			set_state $newstate
		}
	}
}


body tlc::Selector::select {token} { #<<<1
	set selected_token	$token
	if {[info exists inputs($token)]} {
		if {$sense($token)} {
			set_state [expr {!([$inputs($token) state])}]
		} else {
			set_state [$inputs($token) state]
		}
	} else {
		set_state $default
	}
}


body tlc::Selector::explain_txt {{depth 0}} { #<<<1
	return "[string repeat {  } $depth]$this/[expr {($selected_token == {}) ? "default" : $selected_token}] E([info exists inputs($selected_token)])=[expr {[info exists inputs($selected_token)] ? $inputs($selected_token) : "def$default"}] \"[$this name]\": [$this state]\n"
}


body tlc::Selector::cleanup {token} { #<<<1
	puts stderr "tlc::Selector::cleanup: name: ($name) token: ($token)"
	detach_input $token
}


body tlc::Selector::input_debug {lvl msg} { #<<<1
	puts stderr "tlc::Selector::input_debug: lvl: ($lvl) msg: ($msg)"
}


