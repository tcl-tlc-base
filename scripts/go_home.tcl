proc tlc::go_home {} {
	return {
		set tlc::initial_cwd	[pwd]
		set tlc::scriptpath		"./"
		set tlc::script  		[info script]
		if {$tlc::script == ""} {
			set tlc::script  [info nameofexecutable]
		}
		cd [file dirname $tlc::script]
		if {$tlc::script != ""} {
			while {![catch {set tlc::script  [file readlink $tlc::script]}]} {}
			set tlc::scriptpath      [file dirname $tlc::script]
			lappend auto_path   	$tlc::scriptpath
			cd $tlc::scriptpath
		}
	}
}


