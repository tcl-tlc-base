-- vim: foldmarker=<<<,>>>

drop table hconfig_rel;
create table hconfig_rel (
		path		text,
		attrib		text,
		value		text,
		primary key(path,attrib)
);
---- SQL standard does not require an index on primary key, and later postgres
---- versions do not create one automatically
-- create index hconfig_rel_idx on hconfig_rel(path,attrib);

drop function get_field(text,text,text);
create or replace function get_field(text,text,text) returns text as ' #<<<
#	array unset GD plan
	if {![info exists GD(plan)]} {
		set GD(plan)	[spi_prepare "select data,leaf from hconfig where path = \\$1" {text}]
	}

	set path	[string trim $1 "/ "]

	set p		""
	set sep		""
	set leaf	0
	array set build	{}
	foreach elem [split $path /] {
		append p	$sep $elem

#		array unset ::hconfig_cache $p

		if {![info exists ::hconfig_cache($p)]} {
			set data		""
			set leaf		0
			set exists	[spi_execp -count 1 $GD(plan) [list $p]]
#			elog NOTICE "queried ($p), got: ($data) leaf: ($leaf) exists: ($exists)"
			set ::hconfig_cache($p)	[list $data $leaf]
		} else {
			set data		[lindex $::hconfig_cache($p) 0]
			set leaf		[lindex $::hconfig_cache($p) 1]
		}
		
		array set build	$data
		set sep		"/"
		if {$leaf} break
	}

	if {[info exists build($2)]} {
		return $build($2)
	} else {
		return $3
	}
' language 'pltcl';

-- >>>
drop function examine_cache(text);
create or replace function examine_cache(text) returns text as ' #<<<
	if {[info exists ::hconfig_cache($1)]} {
		return $::hconfig_cache($1)
	} else {
		return "undef"
	}
' language 'pltcl';

-- >>>
drop trigger invalidate_hconfig_cache_t on hconfig;
drop function invalidate_hconfig_cache();
create or replace function invalidate_hconfig_cache() returns opaque as ' #<<<
	if {[info exists OLD(path)]} {
		array unset ::hconfig_cache $OLD(path)
	}
	if {[info exists NEW(path)]} {
		array unset ::hconfig_cache $NEW(path)
	}
	return OK
' language 'pltcl';

-- >>>

 create trigger invalidate_hconfig_cache_t
 	after update or delete on hconfig
 	for each row execute procedure invalidate_hconfig_cache();

drop trigger update_hconfig_rel_t on hconfig;
drop function update_hconfig_rel();
create or replace function update_hconfig_rel() returns opaque as ' #<<<
	if {![info exists GD(dplan)]} {
		set GD(dplan)	[spi_prepare "delete from hconfig_rel where path like \\$1" {text}]
	}
	if {![info exists GD(iplan)]} {
		set GD(iplan)	[spi_prepare "insert into hconfig_rel (path,attrib,value) values (\\$1,\\$2,\\$3)" {text text text}]
	}
	if {1 || ![info exists GD(splan)]} {
		set GD(splan)	[spi_prepare "select path,data,leaf from hconfig where path like \\$1 order by path asc" {text}]
	}
	if {![info exists GD(lplan)]} {
		set GD(lplan)	[spi_prepare "select data,leaf from hconfig where path = \\$1" {text}]
	}

	proc hconfig_rel_update {spath} {
		upvar GD GD
		set p		""
		set sep		""
		set leaf	0
		array set build	{}
		foreach elem [split $spath /] {
			append p	$sep $elem

			set data		""
			set leaf		0
			set exists	[spi_execp -count 1 $GD(lplan) [list $p]]
			
			if {[catch {
				array set build	$data
			} msg]} {
				elog NOTICE "data: ($data)"
				error $msg
			}
			set sep		"/"
		}

		set basic	[array get build]
		set pathd	[list $spath]
		set datad	[list $basic]
		spi_execp $GD(splan) [list "${spath}%"] {
			while {![string match "[lindex $pathd end]*" $path] && [llength $pathd] > 1} {
				set pathd	[lrange $pathd 0 end-1]
				set datad	[lrange $datad 0 end-1]
			}
			
			array unset tmp
			array set tmp	[lindex $datad end]
			array set tmp	$data

			set flattened	[array get tmp]

			lappend pathd	$path
			lappend datad	$flattened

			foreach {attrib value} $flattened {
				spi_execp $GD(iplan) [list $path $attrib $value]
			}
		}
	}

	switch -- $TG_op {
		"INSERT" {
			spi_execp $GD(dplan) [list "${NEW(path)}%"]
			hconfig_rel_update $NEW(path)
		}

		"UPDATE" {
			spi_execp $GD(dplan) [list "${OLD(path)}%"]
			if {$OLD(path) != $NEW(path)} {
				spi_execp $GD(dplan) [list "${NEW(path)}%"]
			}
			hconfig_rel_update $NEW(path)
		}

		"DELETE" {
			spi_execp $GD(dplan) [list "${OLD(path)}%"]
		}
	}

	return OK
' language 'pltcl';

 create trigger update_hconfig_rel_t
 	after insert or update or delete on hconfig
 	for each row execute procedure update_hconfig_rel();

-- Force a complete regeneration of hconfig_rel
update hconfig set data = data where path = '';
vacuum analyze hconfig_rel;
