# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

proc tlc::fake_mainloop {} {
	while {1} {
		after 1000000000 {set ::fake_mainloop 0}
		vwait ::fake_mainloop
	}
}


