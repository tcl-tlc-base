# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

proc tlc::readfile {fn {mode "text"}} { #<<<
	set fp	[open $fn r]
	switch -- $mode {
		text {}

		binary {
			fconfigure $fp -encoding binary -translation binary
		}

		default {
			error "Invalid mode: \"$mode\"" "" [list invalid_mode $mode]
		}
	}

	set dat	[read $fp]
	close $fp

	return $dat
}

#>>>
proc tlc::writefile {fn dat {mode "text"}} { #<<<
	set fp	[open $fn w]
	switch -- $mode {
		text {}

		binary {
			fconfigure $fp -encoding binary -translation binary
		}

		default {
			error "Invalid mode: \"$mode\"" "" [list invalid_mode $mode]
		}
	}

	puts -nonewline $fp $dat
	close $fp
}

#>>>
