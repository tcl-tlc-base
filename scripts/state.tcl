# state {widget ...} [enabled | disabled]
# intelligently does a config -state on each of the widgets listed, 
# setting their background colour to the option database key 
# "disabledBackground" or "enabledBackround", or sensible defaults if
# these are not defined.  If the widget's class is not one that takes
# kindly to having it's background set (checkbuttons, etc) then this
# command leaves it alone.  "enabled" is a synonym for "normal"
#
# if $args is blank, it returns the current state (enabled;disabled) of
# the widget.  active is treated as enabled.
#
proc tlc::state {wins args} {
	foreach w $wins {
		if {[winfo exists $w]} {
			if {[llength $args] > 0} {
				set req	[lindex $args 0]
				if {$req == "disabled"} {
					set state	"disabled"
				} elseif {$req == "normal" || $req == "enabled"} {
					set state	"normal"
				} else {
					error "$req: expected \['enabled' | 'disabled'\]"
				}
				catch {$w config -state $state}
				switch -exact $state {
					"normal" {
						set bg	[option get . enabledBackground ""]
						if {$bg == ""} { set bg	"white" }
					}
					"disabled" {
						set bg	[option get . disabledBackground ""]
						if {$bg == ""} { set bg	"grey" }
					}
				}
				set class	[winfo class $w]
				if {[lsearch -exact "Entry Text Listbox" $class] != -1} {
					$w config -background $bg
				} elseif {[lsearch -exact "Entryfield Spinint Labeledtextbox Scrolledtext Vartextbox" $class] != -1} {
					if {$state == "disabled"} {
						set fg	$bg
					} else {
						set fg	black
					}
					$w config -textbackground $bg -foreground $fg
				}
			} else {
				return [$w cget -state]
			}
		} else {
			error "$w is not a valid widget"
		}
	}
}


proc tlc::setstate {cond args} {
	uplevel [list if $cond "state \"$args\" enabled" else "state \"$args\" disabled"]
}


