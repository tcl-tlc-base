# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

proc tlc::stackdump {} { #<<<
	return "Disfunctional at the moment"
	set build		""
	set stackframes	[tlc::build_stackdump 2]

	foreach frameinfo $stackframes {
		if {[dict get $frameinfo type] eq "precompiled"} {
			append build	"compiled - no info" \n
			continue
		}

		if {[dict get $frameinfo type] eq "eval"} {
			continue
		}

		set line	{}

		if {[dict exists $frameinfo file]} {
			lappend line	"([dict get $frameinfo file]:[dict get $frameinfo line])"
		} elseif {[dict exists $frameinfo proc]} {
			lappend line	"(proc [dict get $frameinfo proc]:[dict get $frameinfo line])"
		}

		if {[dict exists $frameinfo desc]} {
			lappend line	[dict get $frameinfo desc]
		} else {
			# TODO: defend against very long values of cmd
			lappend line	[dict get $frameinfo cmd]
		}

		append build	[join $line " "] "\n"
	}

	return $build
}

#>>>
proc tlc::build_stackdump {{trim 2}} { #<<<
	set stackframes	{}

	set thislevel	[expr {[info frame] - $trim}]
	for {set i $thislevel} {$i > 0} {incr i -1} {
		set frameinfo	[info frame $i]
		if {[dict exists $frameinfo level]} {
			set level	[dict get $frameinfo level]
			if {![catch {
				dict set frameinfo levelinf [info level [expr {0 - $level}]]
			}]} {
				set stackname	[lindex [dict get $frameinfo levelinf] 0]
				set passed_args	[lrange [dict get $frameinfo levelinf] 1 end]

				if {$stackname ne "" && $stackname ne "namespace"} {
					if {[catch {
						set fqname \
								[uplevel $level [list namespace origin $stackname]]
					} errmsg]} {
						set fqname "??$stackname"
					}
					if {[catch {
						set caller_args_def \
								[uplevel $level [list info args $stackname]]
					} errmsg]} {
						set caller_args_def	"Bad uplevel depth: ($level)"
					}
				} else {
					set fqname	"<unknown>"
					set caller_args_def	{}
					for {set j	1} {$j < [llength $passed_args]} {incr j} {
						lappend caller_args_def "<unknown$j>"
					}
				}
				set idx		-1
				set substmap    [list "\n" "\\n" "\t" "\\t"]
				set argdesc	{}
				foreach arg $caller_args_def {
					incr idx
					if {$arg eq "args"} {
						set this_passed_arg	[lrange $passed_args $idx end]
					} else {
						set this_passed_arg	[lindex $passed_args $idx]
					}

					set alen	[string length $this_passed_arg]

					if {$this_passed_arg eq ""} {
						set this_passed_arg "{}"
					} elseif {![string is print $this_passed_arg]} {
						set this_passed_arg	"#<nonprint($alen)>"
					} elseif {$alen > 23} {
						set this_passed_arg	[string range $this_passed_arg 0 21]
						set this_passed_arg	[string map $substmap $this_passed_arg]
						set this_passed_arg	"$this_passed_arg/$alen"
					} else {
						set this_passed_arg	[string map $substmap $this_passed_arg]
					}

					lappend argdesc "$arg<$this_passed_arg>"
				}
				set desc	"${fqname}([join $argdesc])"
				dict set frameinfo desc $desc
			}

			lappend stackframes	$frameinfo
		}
	}

	return $stackframes
}

#>>>
