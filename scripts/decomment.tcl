proc tlc::decomment {text} {
	if {[string first "#" $text] == -1} {return $text}

	set build		{}
	foreach line [split $text \n] {
		if {[string index [string trim $line] 0] == "#"} continue
		set idx		[string first "# " $line]
		if {$idx == -1} {
			lappend build $line
		} else {
			incr idx	-1
			lappend build	[string range $line 0 $idx]
		}
	}

	return [join $build \n]
}

proc tlc::splitcomments {text} {
#   the aim of this proc is to preserve comments for a text file.
#   	the output will be a hash array where element 0 is the
#   	stripped config file, and elements thereafter are indexed
#   	per line and constitute the comments on that line
	if {[string first "#" $text] == -1} {
		set ret(0) $text
		return [array get ret]
	}

	set build		{}
	set linenum		1
	foreach line [split $text \n] {
		if {[string index [string trim $line] 0] == "#"} {
			set ret($linenum) "\n$line"
		} else {
			set idx		[string first "# " $line]
			if {$idx == -1} {
				set ret(0) "$ret(0)\n$line"
			} else {
				set tmp [split $line "#"]
				set ret(0)			"$ret(0)\n[lindex $tmp 0]" 
				set ret($linenum)	"#[lindex $tmp 1]"
			}
		}
		incr linenum
	}

}

proc tlc::rebuildcomments {arr_text} {
#	this proc is to perform the reverse of the above procedure.
#		it expects the result of an [array get] to be passed, since
#		it will be working with an array internally
#		the procedure assumes that line numbers have not been altered
#		in between the call above and this one, or that the input
#		array has been compensated for line number changes
#		As you could see from examination of the proc above,
#		comments that are on the same line as useful text are 
#		distinguished from whole-line comments by the fact that 
#		they have no leading \n.

	set conftext [lindex arr_text 1]
	set comments [lrange arr_text 2 end]
	set offset 0
	foreach {line comment} $comments {
		if {[string first "\n" $comment] == 0} {
			set conftext [linsert $conftext [expr {$line+$offset}] $comment]
			incr offset
		} else {
			set conftext [lreplace $line $line "[lindex $conftext $line] $comment"]
		}
	}
	return $conftext
}
