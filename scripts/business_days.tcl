# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

class tlc::Businessdays {
	inherit tlc::Baselog

	constructor {args} {}

	public {
		variable workday_start	"08:00am"
		variable workday_end	"05:00pm"

		variable is_holiday	{}		;# called with epoch secs of day, return 1 if holiday

		method offset {interval {from "now"}}
	}

	private {
		variable workday_start_offset
		variable workday_end_offset
	}
}


configbody tlc::Businessdays::workday_start { #<<<
	set workday_start_offset	[expr {[clock scan $workday_start] - [clock scan "00:00am"}]
}

#>>>
configbody tlc::Businessdays::workday_end { #<<<
	set workday_end_offset	[expr {[clock scan $workday_end] - [clock scan "00:00am"}]
}

#>>>
body tlc::Businessdays::constructor {args} { #<<<
	configure {*}$args
}

#>>>
body tlc::Businessdays::offset {interval {from "now"}} { #<<<
	if {$from eq "now"} {
		set from	[clock seconds]
	} else {
		if {![string is digit -strict $from]} {
			set from	[clock scan $from]
		}
	}

	set month	[clock format $from -format "%b %Y"]
	set daystart	[clock scan "1 $month"]
	set workday_start_offset	[expr {[clock scan "1 $month $workday_start"] - $daystart}]
	set workday_end_offset		[expr {[clock scan "1 $month $workday_end"] - $daystart}]

	set initial	[clock add $from {*}$interval]
	set remaining	[expr {$initial - $from}]
	if {$remaining < 0} {
		error "Negative intervals not allowed"
	}

	set pointer	$from
	set daystart	[clock scan [clock format $pointer -format "%Y-%m-%d"] -format "%Y-%m-%d"]
	while {1} {
		while {$daystart <= $pointer - 86400} {
			incr daystart 86400
		}
		if {$is_holiday ne ""} {
			if {[uplevel #0 $is_holiday [list $daystart]]} {
				incr pointer 86400
				continue
			}
		}
		set dow	[clock format $pointer -format "%a"]
		set gaps	{}
		switch -- $dow {
			"Mon" -
			"Tue" -
			"Wed" -
			"Thu" -
			"Fri" {
				lappend gaps	[list \
						$daystart \
						[expr {$daystart + $workday_start_offset - 1}] \
				]
				lappend gaps	[list \
						[expr {$daystart + $workday_end_offset + 1}] \
						[expr {$daystart + 86400 - 1}] \
				]
			}

			"Sat" - 
			"Sun" {
				lappend gaps	[list \
						$daystart \
						[expr {$daystart + 86400 - 1}] \
				]
			}
		}

		foreach gap $gaps {
			lassign $gap gap_start gap_end
			if {$pointer >= $gap_start && $pointer <= $gap_end} {
				set pointer	[expr {$gap_end + 1}]
			}
		}

		set last_gap_end	[lindex $gaps end 1]
		if {$pointer == $last_gap_end + 1} continue

		if {$remaining <= 0} break

		if {[info exists last_end]} {
			unset last_end
		}
		foreach gap $gaps {
			lassign $gap gap_start gap_end
			if {[info exists last_end] && $gap_start > $pointer} {
				set chunk		[expr {$gap_start - $pointer - 1}]
				if {$remaining >= $chunk} {
					set pointer		$gap_start
					set remaining	[expr {$remaining - $chunk}]
				} else {
					set pointer		[expr {$pointer + $remaining}]
					set remaining	0
				}
				break
			} else {
				set last_end	$gap_end
			}
		}
	}

	return $pointer
}

#>>>
