if {[catch {lsearch -sorted {a b c} b}]} {
	proc tlc::intersect3 {list1 list2} {
		set firstonly		{}
		set intersection	{}
		set secondonly		{}

		set list1	[lsort -unique $list1]
		set list2	[lsort -unique $list2]

		foreach item $list1 {
			if {$item ni $list2} {
				lappend firstonly $item
			} else {
				lappend intersection $item
			}
		}

		foreach item $list2 {
			if {$item ni $intersection} {
				lappend secondonly $item
			}
		}

		return [list $firstonly $intersection $secondonly]
	}
} else {
	proc tlc::intersect3 {list1 list2} {
		set firstonly       {}
		set intersection    {}
		set secondonly      {}

		set list1	[lsort -unique $list1]
		set list2	[lsort -unique $list2]

		foreach item $list1 {
			if {[lsearch -sorted $list2 $item] == -1} {
				lappend firstonly $item
			} else {
				lappend intersection $item
			}
		}

		foreach item $list2 {
			if {[lsearch -sorted $intersection $item] == -1} {
				lappend secondonly $item
			}
		}


		return [list $firstonly $intersection $secondonly]
	}
}


proc tlc::afast_intersect3 {list1 list2} {
    set firstonly       {}
    set intersection    {}
	set map				{}

	set list1	[lsort -unique $list1]
	set list2	[lsort -unique $list2]
	
	foreach i $list2 {
		lappend map	$i 1
	}

	array set b $map

    foreach i $list1 {
        if {[info exists b($i)]} {
            lappend intersection $i
			unset b($i)
        } else {
            lappend firstonly $i
        }
    }

    return [list $firstonly $intersection [array names b]]
}


