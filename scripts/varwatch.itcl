# vim: foldmarker=<<<,>>>

# Signals:
#	onchange()					- Fired when a variable changes value
#	onchange_info(n1, n2, op)	- Fired when a variable changes value

class tlc::Varwatch {
	inherit tlc::Signal

	constructor {accessvar args} {
		upvar $accessvar scopevar
		eval tlc::Signal::constructor scopevar
	} {}
	destructor {}

	public {
		method attach_dirtyvar {varname}
		method detach_dirtyvar {varname}
		method arm {}
		method disarm {}
		method is_armed {}
		method force_if_pending {}
	}

	protected {
		method _on_set_state {pending}
	}

	private {
		variable watchvars		{}
		variable lock			0
		variable afterids		{}
		variable dominos

		method var_update {n1 n2 op}
		method fire_onchange {n1 n2 op}
	}
}


body tlc::Varwatch::constructor {accessvar args} { #<<<1
	array set dominos	{}

	tlc::Domino #auto dominos(onchange) -name "$this onchange"

	configure {*}$args

	$dominos(onchange) attach_output [code $this invoke_handlers onchange]
}


body tlc::Varwatch::destructor {} { #<<<1
	foreach var $watchvars {
		detach_dirtyvar $var
	}
	foreach afterid $afterids {
		after cancel $afterid
	}
	set afterids	{}
}


body tlc::Varwatch::attach_dirtyvar {varname} { #<<<1
	set idx		[lsearch $watchvars $varname]
	if {$idx == -1} {
		lappend watchvars	$varname
	}
	trace variable $varname wu [code $this var_update]
}


body tlc::Varwatch::detach_dirtyvar {varname} { #<<<1
	set idx			[lsearch $watchvars $varname]
	set watchvars	[lreplace $watchvars $idx $idx]
	trace vdelete $varname wu [code $this var_update]
}


body tlc::Varwatch::arm {} { #<<<1
	incr lock -1
}


body tlc::Varwatch::disarm {} { #<<<1
	incr lock
}


body tlc::Varwatch::var_update {n1 n2 op} { #<<<1
	if {$lock > 0} return
	set_state 1
	if {[handlers_available onchange_info]} {
		lappend afterids	[after idle [code $this fire_onchange $n1 $n2 $op]]
	}
	$dominos(onchange) tip
}


body tlc::Varwatch::force_if_pending {} { #<<<1
	set pending		$afterids
	set afterids	{}
	foreach afterid $pending {
		set script	[lindex [after info $afterid] 0]
		after cancel $afterid
		if {$script ne ""} {
			tlc::try {
				uplevel #0 $script
			} onerr {
				default {STDMSG}
			}
		}
	}
	$dominos(onchange) force_if_pending
}


body tlc::Varwatch::fire_onchange {n1 n2 op} { #<<<1
	invoke_handlers onchange_info $n1 $n2 $op
}


body tlc::Varwatch::is_armed {} { #<<<1
	return [expr {($lock > 0) ? 0 : 1}]
}


body tlc::Varwatch::_on_set_state {pending} { #<<<1
	set pending		$afterids
	set afterids	{}
	foreach afterid $pending {
		after cancel $afterid
	}
}


