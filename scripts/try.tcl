# vim: ft=tcl foldmethod=marker foldmarker=<<<,>>> ts=4 shiftwidth=4

package require Tcl 8.5

proc tlc::try {code args} { #<<<
	upvar _tlc_try_standard_actions std_actions
	if {[info exists std_actions]} {
		set actions		[dict merge $std_actions $args]
	} else {
		set actions		$args
	}
	if {[set ecode [catch {uplevel $code} errmsg options]]} {
		if {$ecode != 1} {
			dict incr options -level 1
			return -options $options $errmsg
		}
		if {[dict exists $actions onerr]} {
			set handlers	[string map {STDMSG {log error "\nUnexpected error: $errmsg\n$::errorInfo"}} [dict get $actions onerr]]
			if {![dict exists $handlers default]} {
				dict set handlers default {
					dict incr options -level 1
					return -options $options $errmsg
				}
			}
			if {[set hcode [catch {
				uplevel [list set errmsg	$errmsg]
				uplevel [list set options	$options]
				uplevel [list switch -- [lindex $::errorCode 0] $handlers]
			} herrmsg options]]} {
				dict incr options -level 1
				return -options $options $herrmsg
			}
		}

		if {[dict exists $actions aftererr]} {
			if {[set ecode [catch {uplevel [dict get $actions aftererr]} errmsg options]]} {
				dict incr options -level 1
				return -options $options $errmsg
			}
		}
	} else {
		if {[dict exists $actions onok]} {
			if {[set ecode [catch {uplevel [dict get $actions onok]} errmsg options]]} {
				dict incr options -level 1
				return -options $options $errmsg
			}
		}
	}

	if {[dict exists $actions after]} {
		if {[set ecode [catch {uplevel [dict get $actions after]} errmsg options]]} {
			dict incr options -level 1
			return -options $options $errmsg
		}
	}
}

#>>>
proc tlc::try_set_standard_actions {args} { #<<<
	upvar _tlc_try_standard_actions std_actions
	set std_actions	[dict merge $std_actions $args]
}

#>>>
